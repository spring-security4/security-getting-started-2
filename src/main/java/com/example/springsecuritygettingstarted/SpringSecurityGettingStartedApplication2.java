package com.example.springsecuritygettingstarted;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityGettingStartedApplication2 {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityGettingStartedApplication2.class, args);
    }

}
