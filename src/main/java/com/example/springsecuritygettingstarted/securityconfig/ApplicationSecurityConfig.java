package com.example.springsecuritygettingstarted.securityconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Burda biz UserDetailsService de ozumuze mexsus username password ve role yazmaliyiq
     * bu field lari doldurmaq mutleqdir ve password hissesini adi sekilde yazmaq yox password encoder den istifade etmek lazimdi
     * eks teqdirde " There is no PasswordEncoder mapped for the id "null" " bele bir exception tullayacaq
     * bunun ucun ilk once PasswordEncoder in bean nini yaratmaq lazimdir bundan sonra yazacagimiz passwordu encode edib set
     * etmeliyik
     *
     */
    private final PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() // burda her hansisa bir login teleb etmiyecek
                .anyRequest().authenticated() // qalan butun endpoint lar ucun login olmaginizi teleb edecek
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails elchin = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("el349")
                .password(passwordEncoder.encode("1234"))
                .roles("ADMIN")
                .build();

        return new InMemoryUserDetailsManager(
                elchin
        );
    }
}
